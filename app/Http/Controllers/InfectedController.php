<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Infected;
use App\Models\Item;
use App\Http\Requests;
use DB;

class InfectedController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'infected' => 'required'
        ]);

        $infected = new Infected();
        $infected->survivor_id = $request->infected;
        $infected->save();

        return redirect('/');
    }

    public function index()
    {
        $survivors = DB::table('survivors')->select(DB::raw('count(id) as survivor_count'))
                     ->first();

        $infectedsTable = DB::table('infecteds')->select(DB::raw('count(id) as infected_count'))
                     ->groupBy('survivor_id')
                     ->get();

        $infecteds = 0;

        foreach ($infectedsTable as $value) {
            if($value->infected_count == 3) {
                $infecteds ++;
            }
        }

        $noinfecteds = (($survivors->survivor_count - $infecteds) / $survivors->survivor_count) * 100;
        $infecteds = ($infecteds / $survivors->survivor_count) * 100;

        $items = Item::all();

        foreach ($items as $i => $item) {
            $itemsOnly[$i] = DB::table('inventories')->select(DB::raw('sum(amount) as amount_sum'))
                    ->where('item_id', $item->id)
                    ->groupBy('survivor_id')
                    ->first();
        }

        foreach ($itemsOnly as $i => $item) {
            $itemPerSurvivor[$i]['name'] = $items[$i]->name;
            $itemPerSurvivor[$i]['valor'] = $item->amount_sum / $survivors->survivor_count;
        }
        
        return view('statistics/index', [
            'noinfecteds' => $noinfecteds,
            'infecteds' => $infecteds,
            'items' => $itemPerSurvivor
        ]);
    }

}
