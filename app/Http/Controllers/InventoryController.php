<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventory;
use App\Http\Requests;
use App\Models\Item;
use DB;

class InventoryController extends Controller
{
    public function index(Request $request)
    {
        $inventory = DB::table('inventories')
            ->join('items', 'items.id', '=', 'inventories.item_id')
            ->join('survivors', 'survivors.id', '=', 'inventories.survivor_id')
            ->select('items.name', 'inventories.amount', 'items.id AS item_id', 'items.points')
            ->where('inventories.survivor_id', $request->id)
            ->get();
        echo json_encode($inventory);
    }

    public function update(Request $request)
    {
        // Getting all post data
        if($request->ajax()){

            for ($i=0; $i < 4; $i++) { 
                $item = Inventory::where('survivor_id','=',$request->id1)->where('inventories.item_id', '=' ,$request->itemsexchangeid['1'.$i])->get()->first();


                    $item->amount = ($item->amount - $request->itemsamount['1'.$i]) + $request->itemsamount['2'.$i];

                    $item->save();
            }

            for ($i=0; $i < 4; $i++) { 
                $item = Inventory::where('survivor_id','=',$request->id2)->where('inventories.item_id', '=' ,$request->itemsexchangeid['2'.$i])->get()->first();


                    $item->amount = ($item->amount - $request->itemsamount['2'.$i]) + $request->itemsamount['1'.$i];

                    $item->save();
            }
        }
    }
}
