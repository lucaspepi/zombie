<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Models\Location;
use App\Http\Requests;

class LocationController extends Controller
{
    public function update(Request $request)
    {
        // Getting all post data
	    if($request->ajax()){
            $survivor = Location::where('survivor_id','=',$request->id)->get()->first();
            $survivor->latitud = $request->latitud;
            $survivor->longitud = $request->longitud;
            $survivor->save();
            return "Save";
        }
    }
}
