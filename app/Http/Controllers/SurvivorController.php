<?php

namespace App\Http\Controllers;

use App\Models\Survivor;
use App\Http\Requests;
use App\Models\Location;
use App\Models\Inventory;
use App\Models\Infected;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SurvivorRepository;
use App\Models\Item;
use DB;

class SurvivorController extends Controller
{
    protected $survivors;

    public function __construct(SurvivorRepository $survivors)
    {
        $this->survivors = $survivors;
    }

    public function index()
    {
        $survivors = Survivor::with('location')->get();

        $infecteds = DB::table('infecteds')->select(DB::raw('count(id) as infected_count, survivor_id'))
                     ->groupBy('survivor_id')
                     ->get();

        $items = Item::all();

        return view('welcome', [
            'survivors' => $survivors,
            'items' => $items,
            'infecteds' => $infecteds
        ]);
    }

    /**
     * Create a new task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'age' => 'required|max:255',
            'gender' => 'required|max:255'
        ]);

        $survivor = new Survivor();
        $survivor->name = $request->name;
        $survivor->age = $request->age;
        $survivor->gender = $request->gender;
        $survivor->save();

        $location = new Location();
        $location->survivor_id = $survivor->id;
        $location->latitud = $request->latitud;
        $location->longitud = $request->longitud;
        $location->save();

        $cont = count($request->id);

        for ($i=0; $i < $cont; $i++) { 
            $inventory = new Inventory();
            $inventory->survivor_id = $survivor->id;
            $inventory->item_id = $request->id[$i];
            $inventory->amount = $request->amount[$i];
            $inventory->save();
        }

        return redirect('/');
    }
}
