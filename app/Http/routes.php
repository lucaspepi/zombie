<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SurvivorController@index');


Route::get('/welcome', 'SurvivorController@index');
Route::get('/survivors', 'SurvivorController@index');
Route::post('/survivor', 'SurvivorController@store');
Route::post('/infected', 'InfectedController@store');
Route::post('/location', 'LocationController@update');
Route::delete('/survivor/{survivor}', 'SurvivorController@destroy');
Route::post('/inventory', 'InventoryController@index');
Route::post('/update', 'InventoryController@update');
Route::get('/statistics', 'InfectedController@index');