<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Infected extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survivor_id'
    ];

    public function survivors()
    {
        return $this->hasMany(Survivor::class);
    }
}
