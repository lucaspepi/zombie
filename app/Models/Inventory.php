<?php

namespace App\Models;

use App\Models\Survivor;
use App\Models\Item;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = ['id_survivor', 'id_item', 'amount'];
	
    public function survivors()
    {
        return $this->hasMany(Survivor::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
