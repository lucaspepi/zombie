<?php

namespace App\Models;

use App\Models\Inventory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'points'];
	
    public function inventories()
    {
        return $this->hasOne(Inventory::class);
    }
}
