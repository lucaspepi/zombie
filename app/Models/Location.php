<?php

namespace App\Models;

use App\Models\Survivor;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $fillable = ['id_survivor', 'latitud', 'longitud'];
	
    public function survivors()
    {
        return $this->hasMany(Survivor::class);
    }
}
