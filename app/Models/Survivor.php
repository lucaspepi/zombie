<?php

namespace App\Models;

use App\Models\Location;
use App\Models\Infected;
use Illuminate\Database\Eloquent\Model;

class Survivor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'age', 'gender'];

    public function location()
    {
        return $this->hasOne(Location::class);
    }

    public function infected()
    {
        return $this->hasOne(Infected::class);
    }
}
