<?php

namespace App\Repositories;

use App\Survivor;
use App\Location;

class SurvivorRepository
{
    public function forLocation(Location $location)
    {
        return Survivor::where('survivor_id', $location->survivor_id)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}