function updateLocation(id) {
    var dados = "latitud=" + $("#latitud"+id+"").val() + "&longitud=" + $("#longitud"+id+"").val() + "&id=" + id;
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
    });
    $.ajax({
        url: 'location',
        type: 'post',
        data: dados,
        dataType: 'json',
        success: function(resp){
            location.reload();
        },
        error: function(resp){
            location.reload();
        }
    });
}

function getItems(id, survivor = 0) {
    var dados = "id=" + id;
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
    });
    $.ajax({
        url: 'inventory',
        type: 'post',
        data: dados,
        dataType: 'json',
        success: function(resp){
            $('.items'+survivor+'').append('<input name="id'+survivor+'" type="hidden" value='+id+'>');
            $( resp ).each(function( index ) {
                $('.items'+survivor+'').append('<input name="points['+survivor+index+']" type="hidden" value='+resp[index].points+'><input name="itemsexchangeid['+survivor+index+']" type="hidden" value='+resp[index].item_id+'><span>'+resp[index].name+': '+resp[index].amount+'</span><input class="input-items'+survivor+'" name="itemsamount['+survivor+index+']" type="text" value="">');
            });
            $('.items'+survivor+'').append('<span>Total: </span><input name="total'+survivor+'" type="hidden" value="">');

        },
        error: function(resp){
            $( resp ).each(function( index ) {
                $('.items'+survivor+'').append('<input name="points['+survivor+index+']" type="hidden" value='+resp[index].points+'><input name="itemsexchangeid['+survivor+index+']" type="hidden" value='+resp[index].item_id+'><span>'+resp[index].name+': '+resp[index].amount+'</span><input class="input-items'+survivor+'" name="itemsamount['+survivor+index+']" type="text" value="">');
            });
            $('.items'+survivor+'').append('<span>Total: </span><input name="total'+survivor+'" type="hidden" value="">');
        }
    });
}


function exchange() {
    var dados = $("#item-exchange1").serialize();

    dados += "&" + $("#item-exchange2").serialize();

    var points1 = 0;

    var survivor = 1;

    for (var i = 0; i < 4; i++) {
        points1 += $('input[name="points['+survivor+i+']"]').val() * $('input[name="itemsamount['+survivor+i+']"]').val();
    }
    var points2 = 0;

    var survivor = 2;

    for (var i = 0; i < 4; i++) {
        points2 += $('input[name="points['+survivor+i+']"]').val() * $('input[name="itemsamount['+survivor+i+']"]').val();
    }

    if(points1 > points2) {
        $(".msg").html("Survivor 1 have more points");
    }

    else if (points1 < points2) {
        $(".msg").html("Survivor 2 have more points");
    }

    else {
        $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
        });

        $.ajax({
            url: 'update',
            type: 'post',
            data: dados,
            dataType: 'html',
            success: function(resp){
                console.log(resp);
            },
            error: function(resp){
                console.log(resp);
            }
        });
    }
}

function getItemsInventory(id, survivor = 0) {
    $(".itemsInventory").empty();
    var dados = "id=" + id;
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
    });
    $.ajax({
        url: 'inventory',
        type: 'post',
        data: dados,
        dataType: 'json',
        success: function(resp){
            $( resp ).each(function( index ) {
                $('.itemsInventory').append('<div class="row"><input name="points['+index+']" type="hidden" value='+resp[index].points+'><input name="itemsexchangeid['+index+']" type="hidden" value='+resp[index].item_id+'><span>'+resp[index].name+': '+resp[index].amount+'</span></div>');
            });
            $('#modal4').openModal();
        },
        error: function(resp){
            $( resp ).each(function( index ) {
                $('.items'+survivor+'').append('<input name="points['+survivor+index+']" type="hidden" value='+resp[index].points+'><input name="itemsexchangeid['+survivor+index+']" type="hidden" value='+resp[index].item_id+'><span>'+resp[index].name+': '+resp[index].amount+'</span><input class="input-items'+survivor+index+'" name="itemsamount['+survivor+index+']" type="text" value="">');
            });
            $('.items'+survivor+'').append('<span>Total: </span><input name="total'+survivor+'" type="hidden" value="">');
        }
    });
}

$('#survivor1').change(function() {
    $(".items1").empty();
    getItems($("#survivor1").val(), 1);
});

$('#survivor2').change(function() {
    $(".items2").empty();
    getItems($("#survivor2").val(), 2);
});

$('.btn-exchange').click(function() {
    exchange();
});

/*$( ".input-items1" ).keypress(function() {
    alert();
  console.log( "Handler for .keypress() called." );
});*/