<div id="modal2" class="modal">
  <form action="{{ url('infected') }}" method="POST" class="col s12">
      {!! csrf_field() !!}
  <div class="modal-content">
    <h4>Infected Survivor</h4>
    <div class="row">           
      <div class="row">
        <div class="input-field col s6">
          <select name="survivor">
            @foreach ($survivors as $survivor)
              <option value="{{ $survivor->id }}">{{ $survivor->name }}</option>
            @endforeach
          </select>
          <label>Survivor</label>
        </div>
        <div class="input-field col s6">
          <select name="infected">
            @foreach ($survivors as $survivor)
              <option value="{{ $survivor->id }}">{{ $survivor->name }}</option>
            @endforeach
          </select>
          <label>Infected</label>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn waves-effect waves-light" type="submit" name="action">Save
    </button>
    <a href="#!" class="modal-action modal-close waves-effect waves-light btn">Close</a>
  </div>
  </form>
</div>