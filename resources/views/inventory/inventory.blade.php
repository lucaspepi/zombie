<div id="modal3" class="modal">
      <div class="modal-content">
        <h4>Exchange Items</h4>
        <div class="row">           
          <div class="row">
            <div class="input-field col s6">
              <select name="survivor1" id="survivor1">
                <option value="" disabled selected></option>
                @foreach ($survivors as $survivor)
                  <option value="{{ $survivor->id }}">{{ $survivor->name }}</option>
                @endforeach
              </select>
              <label>Survivor 1</label>
              <div class="input-field col l12">
                <form id="item-exchange1">
                  <div class="items1">
                  </div>
                </form>
              </div>
            </div>
            <div class="input-field col s6">
              <select name="survivor2" id="survivor2">
                <option value="" disabled selected></option>
                @foreach ($survivors as $survivor)
                  <option value="{{ $survivor->id }}">{{ $survivor->name }}</option>
                @endforeach
              </select>
              <label>Survivor 2</label>
              <div class="input-field col l12">
                <form id="item-exchange2">
                  <div class="items2">
                  </div>
                </form>
              </div>
            </div>
            <div class="msg"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn waves-effect waves-light btn-exchange" type="button" name="action">Exchange
        </button>
        <a href="#!" class="modal-action modal-close waves-effect waves-light btn">Close</a>
      </div>
    </div>