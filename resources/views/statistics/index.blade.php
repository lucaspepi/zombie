@extends('layouts.app')

@section('content')
    <!-- Create Task Form... -->
        <!-- Bootstrap Boilerplate... -->

    <div class='container'>
      <div class="row">
          <div class="col l12">
              <h3>Statistics</h3>
              <div class="col l6">
              	<span>Percentage of infected survivors: </span>{{ number_format($noinfecteds) }}%
              </div>
              <div class="col l6">
              	<span>Percentage of non-infected survivors: </span>{{ number_format($infecteds) }}%
              </div>
              @foreach ($items as $i => $item)
              	<div class="col l6">
					         <span>Average amount of </span>{{ $item['name'] }}:
					         {{ number_format($item['valor'], 2) }}
              	</div>
              @endforeach
          </div>
      </div>
    </div>

@endsection