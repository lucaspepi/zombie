<div id="modal1" class="modal">
      <form action="{{ url('survivor') }}" method="POST" class="col s12">
          {!! csrf_field() !!}
      <div class="modal-content">
        <h4>New Survivor</h4>
        <div class="row">
          <div class="row">
            <div class="input-field col s4">
              <input id="first_name" name="name" type="text" class="validate">
              <label for="first_name">Name</label>
            </div>
            <div class="input-field col s4">
              <input id="age" name="age" type="number" class="validate">
              <label for="age">Age</label>
            </div>
            <div class="input-field col s4">
              <select name="gender">
                <option value="" disabled selected></option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
              <label>Gender</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <input id="latitude" name="latitud" type="number" class="validate">
              <label for="latitude">Latitud</label>
            </div>
            <div class="input-field col s6">
              <input id="longitude" name="longitud" type="number" class="validate">
              <label for="longitude">Longitude</label>
            </div>
          </div>
          <div class="row">
            @foreach ($items as $item) 
              <div class="input-field col s3">
                <input id="id" name="id[]" value="{{ $item->id }}" type="hidden" class="validate">
                {{ $item->name }}
              </div>
              <div class="input-field col s3">
                <input id="amount" name="amount[]" type="text" class="validate">
                <label for="latitude">Amount</label>
              </div>
             @endforeach
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn waves-effect waves-light" type="submit" name="action">Save
        </button>
        <a href="#!" class="modal-action modal-close waves-effect waves-light btn btn-save">Close</a>
      </div>
      </form>
    </div>