@extends('layouts.app')

@section('content')
    <!-- Create Task Form... -->
        <!-- Bootstrap Boilerplate... -->

    <div class='container'>
      <div class="row">
          <div class="col l4">
              <h3>Survivors</h3>
          </div>
          <div class="col l8">
              <a class="waves-effect waves-light btn modal-trigger btn-principal" data-target="modal1">New Survivor</a>
              <a class="waves-effect waves-light btn modal-trigger2 btn-principal">Infected</a>
              <a class="waves-effect waves-light btn modal-trigger3 btn-principal">Exchange</a>
          </div>
      </div>
       @if (count($survivors) > 0)
        <table>
          <thead>
            <tr>
                <th data-field="name">Name</th>
                <th data-field="age">Age</th>
                <th data-field="gender">Gender</th>
                <th data-field="latitud">Latitud</th>
                <th data-field="longitud">Longitud</th>
                <th data-field="actions">Actions</th>
            </tr>
          </thead>

          <tbody>
            @foreach ($survivors as $survivor)
              @foreach ($infecteds as $infected)
                @if ($infected->survivor_id == $survivor->id && $infected->infected_count == 3)
                  <tr class="red darken-3">
                @else
                  <tr>
                @endif 
              @endforeach  
                    <td>{{ $survivor->name }}</td>
                    <td>{{ $survivor->age }}</td>
                    <td>{{ $survivor->gender }}</td>
                    <td><input type="number" id="latitud{{ $survivor->id }}" name="latitud[{{ $survivor->id }}]" value="{{ $survivor->location->latitud }}"></td>
                    <td><input type="number" class="latitud" id="longitud{{ $survivor->id }}" name="longitud[{{ $survivor->id }}]" value="{{ $survivor->location->longitud }}"></td>
                    <td>
                      <a onclick="updateLocation({{ $survivor->id }})" class="waves-effect waves-light btn btn-update">Update Location</a>
                      <a onclick="getItemsInventory({{ $survivor->id }})" class="waves-effect waves-light btn">Inventory</a>
                    </td>
                  </tr>
            @endforeach
          </tbody>
        </table>
      @endif
    </div>

    @extends('survivors.survivor')
  
    @extends('inventory.inventory')

    @extends('infected.infected')

    @extends('inventory.index')

  <script>
    $(document).ready(function(){
      // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
      $('.modal-trigger').leanModal();

      $('.modal-trigger2').click(function(){
        $('#modal2').openModal();
      });

      $('.modal-trigger3').click(function(){
        $('#modal3').openModal();
      });

      $('select').material_select();    

    });
  </script>       

@endsection